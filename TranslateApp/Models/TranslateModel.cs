﻿using System.ComponentModel.DataAnnotations;

namespace TranslateApp.Models
{
    public class TranslateViewModel
    {
        [Required]
        [MinLength(2)]
        public string TextForTranslation { get; set; }
    }
}