﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace TranslateApp.Models
{
    [XmlRoot("translations")]
    public class TranslationsSerializable
    {
        public TranslationsSerializable() { Items = new List<TranslationSerializable>(); }

        [XmlElement("translation")]
        public List<TranslationSerializable> Items { get; set; }
    }

    public class TranslationSerializable
    {
        public TranslationSerializable() { }

        [XmlElement("id")]
        public long Id { get; set; }

        [XmlElement("timestamp")]
        public DateTime Timestamp { get; set; }

        [XmlElement("from")]
        public string From { get; set; }

        [XmlElement("to")]
        public string To { get; set; }
    }
}