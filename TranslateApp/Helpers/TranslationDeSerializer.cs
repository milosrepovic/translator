﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using TranslateApp.Models;

namespace TranslateApp.Helpers
{
    public static class TranslationDeSerializer
    {
        private static XmlSerializer serializer;

        static TranslationDeSerializer()
        {
            serializer = new XmlSerializer(typeof(TranslationsSerializable));
        }

        public static void SerializeAndSaveToXML(TranslationsSerializable translationsToSerialize)
        {
            XmlDocument xmlDocument = new XmlDocument();
            using (MemoryStream stream = new MemoryStream())
            {
                serializer.Serialize(stream, translationsToSerialize);
                stream.Position = 0;
                xmlDocument.Load(stream);
                xmlDocument.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "StoredTranslations.xml"));
            }
        }

        public static TranslationsSerializable DeserializeFromXML()
        {
            XmlDocument xmlDocument = new XmlDocument();
            TranslationsSerializable deserializedTranslations;

            xmlDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "StoredTranslations.xml"));
            string xmlString = xmlDocument.OuterXml;

            using (StringReader read = new StringReader(xmlString))
            {
                using (XmlReader reader = new XmlTextReader(read))
                {
                    deserializedTranslations = (TranslationsSerializable)serializer.Deserialize(reader);
                }
            }

            return deserializedTranslations;
        }
    }
}