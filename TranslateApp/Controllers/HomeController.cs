﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using TranslateApp.Helpers;
using TranslateApp.Models;
using YandexTranslateCSharpSdk;

namespace TranslateApp.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> TranslateText(TranslateViewModel vm)
        {
            string result = await Translate(vm.TextForTranslation);
            ViewBag.TranslatedText = result;
            return View();
        }

        private async Task<string> Translate(string textForTranslation)
        {
            string result = string.Empty;
            TranslationsSerializable deserializedTranslations;
            if (!CheckInLocalStorage(textForTranslation, out result, out deserializedTranslations))
            {
                result = await CallTranslationApi(textForTranslation);

                if (!string.IsNullOrEmpty(result))
                {
                    StoreResultInLocalStorage(textForTranslation, result, deserializedTranslations);
                }
            }

            return result;
        }

        private void StoreResultInLocalStorage(string textToTranslate, string result, TranslationsSerializable deserializedTranslations)
        {
            long newResultId = deserializedTranslations.Items.Any() ? deserializedTranslations.Items.Last().Id + 1 : 1;
            deserializedTranslations.Items.Add(new TranslationSerializable()
            {
                Id = newResultId,
                Timestamp = DateTime.Now,
                From = textToTranslate,
                To = result
            });

            TranslationDeSerializer.SerializeAndSaveToXML(deserializedTranslations);
        }

        private bool CheckInLocalStorage(string textForTranslation, out string result, out TranslationsSerializable deserializedTranslations)
        {
            result = string.Empty;
            deserializedTranslations = new TranslationsSerializable();

            if (!string.IsNullOrEmpty(textForTranslation))
            {
                XmlDocument xmlDocument = new XmlDocument();
                if (System.IO.File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "StoredTranslations.xml")))
                {
                    deserializedTranslations = TranslationDeSerializer.DeserializeFromXML();

                    TranslationSerializable foundTranslation = deserializedTranslations.Items.FirstOrDefault(
                        x => string.Equals(x.From.ToLower(), textForTranslation.ToLower(), StringComparison.Ordinal));

                    if (foundTranslation != null)
                    {
                        result = foundTranslation.To;
                        return true;
                    }
                }
                else
                {
                    TranslationDeSerializer.SerializeAndSaveToXML(deserializedTranslations);
                }
            }

            return false;
        }

        private async Task<string> CallTranslationApi(string textForTranslation)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(textForTranslation))
            {
                YandexTranslateSdk wrapper = new YandexTranslateSdk();
                wrapper.ApiKey = ConfigurationManager.AppSettings["APIKey"].ToString(); ;
                string detectedlanguage = await wrapper.DetectLanguage(textForTranslation);
                result = await wrapper.TranslateText(textForTranslation
                    , $"{detectedlanguage}-en");
            }

            return result;
        }
    }
}